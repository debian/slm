from strictyaml.parser import load as yaml_load
import sys, re, os

def translate_metadata(f):
    """
    Renvoie la valeur de la métadonnée "translate" d'un fichier
    markdown
    @return Non, ou False, ou True
    """
    with open(f) as mdfile:
        text = mdfile.read()
        s = re.search(r"(^---.*)^---", text, re.MULTILINE|re.DOTALL)
        if s:
            y = yaml_load(s.group(1))
            return str(y.get("translated")).lower() == "true"
    return None

def not_translated(d, lang=None):
    """
    liste les fichiers contenant la métadonnée translation: false
    @param d un répertoire
    @param lang langue recherchée (facultatif)
    @return le texte des noms de fichiers, un par ligne
    """
    files = [os.path.join(d,f) for f in os.listdir(d)
             if translate_metadata(os.path.join(d,f)) is False]
    if lang:
        files = [f for f in files if re.match(f".*\\.{lang}\\.md", f)]
    return "\n".join(files)

def demo(d):
    files = [os.path.join(d,f) for f in os.listdir(d)]
    print("translation: true")
    print([f for f in files if translate_metadata(f) is True])
    print("translation: false")
    print([f for f in files if translate_metadata(f) is False])
    print("translation: unknown")
    print([f for f in files if translate_metadata(f) is None])
    return

if __name__ == "__main__":
    #demo(sys.argv[1])
    if len(sys.argv) > 2:
        nt = not_translated(sys.argv[1], sys.argv[2])
    else:
        nt = not_translated(sys.argv[1])
    if nt:
        if len(sys.argv) > 2:
            print(f"Fichiers non traduits ({sys.argv[2]}) :")
            print("============================")
            print(nt)
        else:
            print("Fichiers non traduits :")
            print("=======================")
            print(nt)
