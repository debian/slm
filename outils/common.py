# fonctions communes et constantes communes

import re

def safeText(text):
    result=text
    desaccent = {
        r"[àâä]" : "a",
        r"[ÀÂÄ]" : "A",
        r"[éèêë]" : "e",
        r"[ÉÈÊË]" : "E",
        r"[îï]" : "i",
        r"[ÎÏ]" : "I",
        r"[ôö]" : "o",
        r"[ÔÖ]" : "O",
        r"[ùûü]" : "u",
        r"[ÙÛÜ]" : "U",
        r"[ç]" : "c",
        r"[Ç]" : "C",
    }
    for r, repl in desaccent.items():
        result = re.sub(r,repl, result)
    return result
    
def safeIdent(text):
    return re.sub(r"""[. ,;!"'?]""", "_", safeText(text).replace("°", ""))
