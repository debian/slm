#! /usr/bin/python3

"""
Fabrication d'une base de données de test. On reprend une base de
données avec des noms d'élèves réels, qu'on rend anonymes en injectant
des noms issus d'une longue liste de noms factices, et en mélangeant
aléatoirement les prénoms.
"""

import os
import  sqlite3
import  random
import  sys
from unidecode import unidecode

BASEDIR = os.path.dirname(__file__)
BASE = BASEDIR + "/../db.sqlite3"
TEST_BASE = BASEDIR + "/db_test.sqlite3"

if __name__ == "__main__":
    # inutile de copier la base à chaque fois, de toute façon on peut
    # anonymiser plusieurs fois une même base !
    ## shutil.copyfile(BASE, TEST_BASE)
    con = sqlite3.connect(TEST_BASE)
    with con:
        cur = con.cursor()
        prenoms=set()
        for row in cur.execute("SELECT Prenom FROM gestion_eleves"):
            prenoms.add(row[0])
        prenoms = sorted(list(prenoms))
        noms = [n.strip() for n in \
                open("noms-aleatoires.txt").read().split("\n") if n.strip()]
        # print(prenoms, len(prenoms))
        # print(random.choice(prenoms))
        ids=[]
        for row in cur.execute("SELECT id FROM gestion_eleves"):
            ids.append(row[0])
        sql = """UPDATE gestion_eleves SET
          Nom_de_famille=(:nom),
          Nom=(:nom),
          Nom_d_usage=(:nom),
          Prenom=(:prenom),
          Prenom_2='',
          Prenom_3='',
          NomPrenomEleve=(:nomprenom),
          sans_accent=(:sans_accent),
          Tel_Personnel=(:tel),
          Tel_Professionnel=(:tel),
          Tel_Portable=(:tel),
          email='john.doe@example.com',
          Nom_Repr_Leg='Doe',
          Nom_de_famille_Repr_Leg='Doe',
          Nom_d_usage_Repr_Leg='Doe',
          Prenom_Repr_Leg='John',
          ConcaReprLeg='',
          Tel_Personnel_Repr_Leg=(:tel),
          Tel_Portable_Repr_Leg=(:tel),
          Tel_Professionnel_Repr_Leg=(:tel),
          Email_Repr_Leg='daddy.doe@example.com',
          Nom_Autre_Repr_Leg='Doe',
          Nom_de_famille_Autre_Repr_Leg='Doe',
          Nom_d_usage_Autre_Repr_Leg='Doe',
          Prenom_Autre_Repr_Leg='John',
          ConcaAutreReprLeg='',
          Tel_Personnel_Autre_Repr_Leg=(:tel),
          Tel_Portable_Autre_Repr_Leg=(:tel),
          Tel_Professionnel_Autre_Repr_Leg=(:tel),
          Email_Autre_Repr_Leg='mummy.doe@example.com'
          WHERE id=(:id)"""
        sql_modif_caution = "UPDATE gestion_caution SET nom=(:nom), " + \
            "prenom=(:prenom), sans_accent=(:sans_accent) " +\
            "WHERE numero=(:no_caution)"
        for id in sorted(ids):
            no_caution = 0
            for row in cur.execute(
                    f"SELECT no_caution FROM gestion_eleves WHERE id={id}"):
                no_caution = row[0]
            nom = random.choice(noms)
            prenom = random.choice(prenoms)
            nomprenom = nom + ' ' + prenom
            sans_accent = unidecode(nomprenom)
            if no_caution > 0:
                cur.execute(sql_modif_caution,{
                    "nom": nom, "prenom": prenom,
                    "no_caution": no_caution,
                    "sans_accent": sans_accent,
                })
            data= {
                "nom": nom,
                "prenom": prenom,
                "nomprenom": nomprenom,
                "sans_accent": sans_accent,
                "id": id,
                "tel": "06 12 34 56 78"
            }
            cur.execute(sql, data)
            print(".", end="")
            sys.stdout.flush()
        con.commit()
        ######################################
        # efface les sans-caution
        ######################################
        cur.execute("UPDATE gestion_caution SET somme_payee=70, commentaire=''")
        ######################################
        # efface l'importation de siecle
        ######################################
        cur.execute("DELETE FROM gestion_eleves_de_siecle WHERE 1")
        ######################################
        # efface le journal
        ######################################
        cur.execute("DELETE FROM django_admin_log")
        ######################################
        # efface les sessions
        ######################################
        cur.execute("DELETE FROM django_session")
        ######################################
        # conserve un seul compte : admin, mot de passe "slmslm1234",
        # dont le hash est codé en dur ci-dessous
        ######################################
        cur.execute("DELETE FROM auth_user WHERE username != 'admin'")
        cur.execute("UPDATE auth_user set " + \
                    "password='pbkdf2_sha256$600000$7R0oBpiQRVoNMXhmGD" + \
                    "QAPa$uE5MZLYr6SSatUfJYCYdOkWyCrpKtzk682oDtatiz2Y=' " + \
                    "WHERE username='admin'")
        con.commit()
    # fin de with con, équivalent à con.close()
    print()
    print ("modifié la base", TEST_BASE)
        
