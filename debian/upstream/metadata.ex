# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/slm/issues
# Bug-Submit: https://github.com/<user>/slm/issues/new
# Changelog: https://github.com/<user>/slm/blob/master/CHANGES
# Documentation: https://github.com/<user>/slm/wiki
# Repository-Browse: https://github.com/<user>/slm
# Repository: https://github.com/<user>/slm.git
