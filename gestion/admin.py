"""
    gestion.urls, un module pour SLM
    - déclare les tables gérables par l'administrateur

    Copyright (C) 2023 Georges Khaznadar <georgesk@debian.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.contrib import admin

from .models import Classes, Parametres, Disciplines, Niveaux, Filieres, \
    Series, TypeMateriel, Courriers, Commandes, Personnels, \
    Eleves, Eleves_de_siecle, Materiel, Inventaire, Details, Prets, Caution, \
    RevueStock, Jeton, LivreSupplement, Livre_a_lire

admin.site.register(Classes)
admin.site.register(Parametres)
admin.site.register(Disciplines)
admin.site.register(Niveaux)
admin.site.register(Filieres)
admin.site.register(Series)
admin.site.register(TypeMateriel)
admin.site.register(Courriers)
admin.site.register(Commandes)
admin.site.register(Personnels)
admin.site.register(Eleves_de_siecle)
admin.site.register(Inventaire)
admin.site.register(Details)
admin.site.register(Caution)
admin.site.register(Jeton)
admin.site.register(LivreSupplement)
admin.site.register(Livre_a_lire)

class AtributionAdmin(admin.ModelAdmin):
    raw_id_fields = ("eleve", "inventaire")
admin.site.register(Prets, AtributionAdmin)

class RevueAdmin(admin.ModelAdmin):
    raw_id_fields = ("code",)
    list_filter = ("prete",)
admin.site.register(RevueStock, RevueAdmin)


class InitialeFilter(admin.SimpleListFilter):
    """
    Pour afficher un choix par initiales dans la bere latérale de
    l'administration
    """
    title = "Initiale"
    parameter_name = "Première lettre"

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return [
            (c.lower(), c.upper()) for c in "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        ]
    
    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        c = self.value()
        if c:
            return queryset.filter(sans_accent__startswith = c)
        return

class ElevesAdmin(admin.ModelAdmin):
    list_filter = [InitialeFilter]
admin.site.register(Eleves, ElevesAdmin)

class MaterielAdmin(admin.ModelAdmin):
    list_filter = ["discipline", "niveau"]
admin.site.register(Materiel, MaterielAdmin)
