"""
    gestion.version, un module pour SLM
    - Ce petit module renvoie juste le numéro de version, éventuelement
      calculé en examinant des git-tags

    Copyright (C) 2023-2024 Georges Khaznadar <georgesk@debian.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from subprocess import run
from packaging.version import Version

############ modifications must be done here ##############################
major = 1
minor = 3
flavor = ".0"
############ end of modifiable section      ###############################

this_version = f"{major}.{minor}"
if flavor:
    this_version = f"{major}.{minor}{flavor}"


p = run("git tag | grep '^v'| sed 's/^v//'", shell=True, capture_output=True)

versions = [v for v in p.stdout.strip().decode("utf-8").split("\n") if v]
versions = sorted(versions, key = Version)

def create_new_tag(verbose=True):
    """
    Create a vew tag "v"+this_version when it does not exist already
    """
    if (not versions) or Version(this_version) > Version(versions[-1]):
        run(f"git tag v{this_version}", shell=True)
        if verbose:
            print(f"added the tag v{this_version}")

if __name__ == "__main__":
    if (not versions) or Version(this_version) > Version(versions[-1]):
        print(this_version)
        print("already tagged:", " ".join(versions))
    else:
        print(f"The current version is {this_version}, and is in the already tagged versions: {versions}")
    
