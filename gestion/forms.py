from django.forms import ModelForm, Form, PasswordInput, \
   HiddenInput, ValidationError
from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Parametres, Courriers, Disciplines, Materiel, \
   Classes, Eleves

import re

class donnees_utilisateur_Form(Form):
   labelNom = _("Nom")
   nom = forms.CharField(label=labelNom, max_length=25, required = False)
   prenom = forms.CharField(label=_("Prénom"), max_length=25, required = False)
   email = forms.CharField(label=_("Courriel"), max_length=50, required = False)
   passe1 = forms.CharField(label=_("Mot de passe"), max_length=32,
                            widget=PasswordInput, required = False)
   passe2 = forms.CharField(label=_("Répétez le mot de passe"), max_length=32,
                            widget=PasswordInput, required = False)
   next = forms.CharField(max_length=50, widget = HiddenInput)

   def __init__(self, *args, **kwargs):
      super(donnees_utilisateur_Form, self).__init__(*args, **kwargs)
      self.fields['nom'].widget.attrs['placeholder'] = 'Votre nom ?'
      self.fields['prenom'].widget.attrs['placeholder'] = 'Votre prénom ?'
      self.fields['email'].widget.attrs['placeholder'] = 'Votre courriel ?'
      self.fields['passe1'].widget.attrs['placeholder'] = \
         'Nouveau mot de passe éventuel'
      self.fields['passe2'].widget.attrs['placeholder'] = 'Le même ...'
      return
     
   def clean_passe2(self):
      p1 = self.cleaned_data["passe1"]
      p2 = self.cleaned_data["passe2"]
      if p1 and not p2 == p1:
         raise ValidationError(
            _("Erreur : les deux mots de passes étaient différents, recommencez"))
      if p1 and len(p1) < 7:
         raise ValidationError(
            _("Erreur : un mot de passe doit contenir au moins 7 caractères"))
      minuscules = re.findall(r"[a-z]", p1)
      majuscules = re.findall(r"[A-Z]", p1)
      chiffres = re.findall(r"\d", p1)
      if p1 and (not minuscules or not majuscules or not chiffres):
          raise ValidationError(
            _("Erreur : un mot de passe doit contenir au moins ") + \
             _("une lettre minuscule, une majuscule, et un chiffre"))
      return p2


class Parametres_Form(ModelForm):
   class Meta:
        model = Parametres
        fields = '__all__'
        
class Courriers_Form(ModelForm):
   class Meta:
        model = Courriers
        fields = '__all__'
        exclude = ('N',)

class Materiel_Form(ModelForm):
   discipline = forms.ModelChoiceField(queryset=Disciplines.objects.order_by('libelle'))
   class Meta:
      model = Materiel
      fields = '__all__'
      exclude = ('id','Pretes','Achetes','Perdus')
  
class Eleves_Form(ModelForm):
   classe = forms.ModelChoiceField(queryset=Classes.classes_importantes())
   class Meta:
      model = Eleves
      fields = ('Nom_de_famille', 'Prenom', 'classe',
                'classes_passees', 'no_caution',
                'Lib_Mat_Enseignee_1','Lib_Mat_Enseignee_2',
                'Lib_Mat_Enseignee_3','Lib_Mat_Enseignee_4',
                'Lib_Mat_Enseignee_5','Lib_Mat_Enseignee_6',
                'Lib_Mat_Enseignee_7','Lib_Mat_Enseignee_8',
                'Lib_Mat_Enseignee_9','Lib_Mat_Enseignee_10',
                'Tel_Portable', 'Adresse_Repr_Leg',
                'Tel_Portable_Repr_Leg', 'Tel_Portable_Autre_Repr_Leg',
                'a_verifier')
      labels = {f.name: _(f.verbose_name) for f in Eleves._meta.fields}
      widgets = {
         'classes_passees': forms.TextInput(attrs={'readonly': 'readonly'}),
         'no_caution': forms.TextInput(attrs={'readonly': 'readonly'}),
         'a_verifier': forms.TextInput(attrs={'readonly': 'readonly'}),
      }

class Eleves_ancien_Form(ModelForm):
   """
   Formulaire pour les élèves dont le numéro de caution n'avait pas été
   attribué automatiquement par le logiciel, c'est à dire avec un numéro de
   caution inférieur à MIN_NO_CAUTION
   """
   classe = forms.ModelChoiceField(queryset=Classes.classes_importantes())
   class Meta:
      model = Eleves
      fields = ('Nom_de_famille', 'Prenom', 'classe',
                'classes_passees', 'no_caution',
                'Lib_Mat_Enseignee_1','Lib_Mat_Enseignee_2',
                'Lib_Mat_Enseignee_3','Lib_Mat_Enseignee_4',
                'Lib_Mat_Enseignee_5','Lib_Mat_Enseignee_6',
                'Lib_Mat_Enseignee_7','Lib_Mat_Enseignee_8',
                'Lib_Mat_Enseignee_9','Lib_Mat_Enseignee_10',
                'Tel_Portable', 'Adresse_Repr_Leg',
                'Tel_Portable_Repr_Leg', 'Tel_Portable_Autre_Repr_Leg',
                'a_verifier')
      labels = {f.name: _(f.verbose_name) for f in Eleves._meta.fields}
      widgets = {
         'classes_passees': forms.TextInput(attrs={'readonly': 'readonly'}),
         'a_verifier': forms.TextInput(attrs={'readonly': 'readonly'}),
      }

class SiecleForm(forms.Form):
    fichier_siecle = forms.FileField(label='Sélectionner un export de SIÈCLE')
