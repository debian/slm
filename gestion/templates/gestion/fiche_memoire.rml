{% load static %}
{% load i18n %}
<?xml version="1.0" encoding="utf-8" standalone="no" ?> 
<!DOCTYPE document SYSTEM "rml_1_0.dtd">
<document>
  <template showBoundary="0">
    <pageTemplate id="main">
      <pageGraphics>
	<setFont name="Helvetica-Bold" size="9"/>
	<drawString x="1.8cm" y="27.5cm">{% translate "Ministère de l'Éducation Nationale" %}</drawString>
	<drawString x="1.8cm" y="27.15cm">{% translate "Académie de" %} {{params.NomAcademie}}</drawString>
	<setFont name="Helvetica-Bold" size="12"/>
	<drawString x="1.8cm" y="26.7cm">{{params.NomEts}}</drawString>
	<setFont name="Helvetica-Bold" size="9"/>
	<drawString x="1.8cm" y="26.3cm">{{params.Adresse}}</drawString>
	<drawString x="1.8cm" y="25.95cm">{{params.CodePostal}} {{params.Commune}}</drawString>
	<drawString x="1.8cm" y="25.6cm">Tél : {{params.Telephone}}</drawString>
	
	<setFont name="Helvetica-Bold" size="18"/>
	<drawString x="4.2cm" y="24.5cm">{% translate "MÉMOIRE pour PERTE de MANUELS SCOLAIRES" %}</drawString>
	<setFont name="Helvetica-Bold" size="9"/>
	<drawString x="9.3cm" y="24cm">{% translate "ou autres matériels" %}</drawString>
	<setFont name="Helvetica-Bold" size="12"/>
	<drawString x="8cm" y="23.6cm">{% translate "Année scolaire" %} {{ params.AnneeScolaire }}</drawString>
	<!--
	<setFont name="Helvetica" size="6"/>
	<drawString x="45" y="45">{% translate "Page" %} <pageNumber/>{% translate ", imprimée le :" %} {{ date }}</drawString>
	-->
	<image file="{{logo}}" x="16.8cm" y="25cm" width="3cm" />
      </pageGraphics>
      <frame id="second" x1="1.8cm" y1="1.8cm" width="17.4cm" height="21.5cm"/>
    </pageTemplate>
  </template>
  <stylesheet>
    <paraStyle name="style1"
	       fontName="Helvetica"
	       fontSize="10"
	       underlineColor="blue"
	       />
    <paraStyle name="style_droite"
	       parent="style1"
	       alignment = "right"
	       fontName="Helvetica-Bold"
	       />
    <blockTableStyle id="t1">
      <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
      <blockBackground colorName="white" start="0,0" stop="-1,0"/>
      {% for m in manuels %}
      {% if forloop.counter|divisibleby:2 %}
      <blockBackground colorName="#BBFFFF" start="0,{{forloop.counter}}" stop="-1,{{forloop.counter}}"/>
      {% else %}
      <blockBackground colorName="#FFFFBB" start="0,{{forloop.counter}}" stop="-1,{{forloop.counter}}"/>
      {% endif %}
      {% endfor %}
    </blockTableStyle>
    <blockTableStyle id="t2">
      <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
      <blockBackground colorName="white" start="0,0" stop="-1,0"/>
      <blockSpan start="0,0" stop="1,0"/>
      {% for l in "12345" %}
      {% if forloop.counter|divisibleby:2 %}
      <blockBackground colorName="#BBFFFF" start="0,{{forloop.counter}}" stop="-1,{{forloop.counter}}"/>
      {% else %}
      <blockBackground colorName="#FFFFBB" start="0,{{forloop.counter}}" stop="-1,{{forloop.counter}}"/>
      {% endif %}
      {% endfor %}
    </blockTableStyle>
  </stylesheet>
  <story>
    <para style="style1">
      {% translate "ÉLÈVE :" %} {{ eleve.NomPrenomEleve }}
    </para>
    <para style="style1">
      {% translate "CLASSE :" %} {{ eleve.Lib_Structure }}, le {{ date }}
    </para>
    <spacer length="0.3cm"/>
    {% if manuels %}
    <blockTable style="t1" colWidths="1.6cm,1.6cm,5.4cm,2.2cm,1.4cm,1.4cm,1.9cm,1.7cm">
      <tr>
	<td><para style="style1"><b>{% translate "Matière" %}</b></para></td>
	<td><para style="style1"><b>{% translate "N°" %}</b></para></td>
	<td><para style="style1"><b>{% translate "Titre" %}</b></para></td>
	<td><para style="style1"><b>{% translate "Date" %}</b></para></td>
	<td><para style="style1"><b>{% translate "État initial" %}</b></para></td>
	<td><para style="style1"><b>{% translate "État final" %}</b></para></td>
	<td><para style="style1"><b>{% translate "Dégradé/Perdu" %}</b></para></td>
	<td><para style="style1"><b>{% translate "Tarif" %}</b></para></td>
      </tr>
      {% for m in manuels %}
      <tr>
	<td data-n="1">
	  <para style="style1">
	    {{m.inventaire.materiel.discipline.abrege}}
	  </para>
	</td>
	<td data-n="2">
	  <para style="style1">
	    {{m.inventaire.id}}
	  </para>
	</td>
	<td data-n="3">
	  <para style="style1">
	    {{m.inventaire.materiel.titre}}
	  </para>
	</td>
	<td data-n="4">
	  <para style="style1">
	    {% if m.date_retour > m.date_pret %}
	    {{m.date_retour|date:"d/m/Y"}}
	    {% endif %}
	    {% if m.inventaire.Perdu %}
	    {{m.Date_Declaration_Perte|date:"d/m/Y"}}
	    {% endif %}
	  </para>
	</td>
	<td data-n="5">
	  <para style="style1">
	    {{m.etat_initial}}
	  </para>
	</td>
	<td data-n="6">
	  <para style="style1">
	    {{m.etat_final}}
	  </para>
	</td>
	<td data-n="7">
	  <para style="style1">
	    {% if m.isDegrade %}
	    {% translate "Dégradé" %}
	    {% endif %}
	    {% if m.inventaire.Perdu %}
	    {% translate "Perdu" %}
	    {% endif %}
	  </para>
	</td>
	<td data-n="8">
	  <para style="style1">
	    {% if m.tarif > 0 %}
	    {{ m.tarif|floatformat:2 }}{% translate " €" %}
	    {% endif %}
	  </para>
	</td>
      </tr>
      {% endfor %}
    </blockTable>
    {% else %}
    <para style="style1">
      {% translate "ERREUR : On n'a pas trouvé dans la base de données de manuel en prêt." %}
    </para>
    {% endif %}
    <spacer length="0.3cm"/>
    <para style="style1">
      {% if nb_non_rendu %}
      {{ nb_non_rendu }} {% translate "Manuel(s) scolaires(s) non rendu(s)" %}
      {% endif %}
    </para>    
    <para style="style_droite">
      {% translate "Total :" %} {{total|floatformat:2}}{% translate " €" %}
    </para>
    <spacer length="0.3cm"/>
    
    <para style="style1">
    {% for l in messages %}
    {{ l }}
    {% endfor %}
    </para>
    <spacer length="0.3cm"/>
    
    <spacer length="0.3cm"/>
    <para style="style_droite">
      {{params.NomAgentComptable}}                           {{params.NomChefEts}}
    </para>
    <spacer length="1.0cm"/>
    <blockTable style="t2" colWidths="15cm, 2.2cm">
      <tr><td><para>{% translate "Suivi du mémoire pour perte de matériel" %}</para></td><td></td></tr>
      <tr><td><para>{% translate "Déduction de la caution" %}</para></td><td><para>☐</para></td></tr>
      <tr><td><para>{% translate "Espèces" %}</para></td><td><para>☐</para></td></tr>
      <tr><td><para>{% translate "Chèques" %}</para></td><td><para>☐</para></td></tr>
      <tr><td><para>{% translate "Acquittée" %}</para></td><td><para>☐</para></td></tr>
    </blockTable>
  </story>
</document>
