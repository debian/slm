"""
    gestion.queries, un module pour SLM
    - quelques facilitateurs pour les requêtes dans la base de données

Copyright (c) 2024 Georges Khaznadar <georgesk@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
from django.db.models import Q

#####################################################################
###           facilitateurs pour gestion.Materiel                 ###
#####################################################################

### les classes de seconde ... pas de filière technologique distincte
mat_2nde = Q(niveau__libelle = "Seconde") | \
    Q(niveau__libelle = "Tous")
# sélection des manuels pouvant convenir à une seconde :
# Materiel.objects.filter(mat_2nde)


### les premières générales
mat_1ere_niveau = Q(niveau__libelle = "Première") | \
    Q(niveau__libelle  = "Première-Terminale") | \
    Q(niveau__libelle = "Tous")
mat_1ere_filiere = Q(filiere__nom = "Générale") | \
    Q(filiere__nom = "Générale et technologique") | \
    Q(filiere__nom = "Toutes")
mat_1ere = mat_1ere_niveau & mat_1ere_filiere
# sélection des manuels pouvant convenir à une première générale :
# Materiel.objects.filter(mat_1ere)

### les premières technologiques
mat_1ere_techno_filiere = Q(filiere__nom = "Technologique") | \
    Q(filiere__nom = "Générale et technologique") | \
    Q(filiere__nom = "Toutes")
mat_1ere_techno = mat_1ere_niveau & mat_1ere_techno_filiere
# sélection des manuels pouvant convenir à une première technologique :
# Materiel.objects.filter(mat_1ere_techno)

### Les terminales générales
mat_tale_niveau = Q(niveau__libelle = "Terminale") | \
    Q(niveau__libelle  = "Première-Terminale") | \
    Q(niveau__libelle = "Tous")
mat_tale = mat_1ere_filiere & mat_tale_niveau
# sélection des manuels pouvant convenir à une terminale générale :
# Materiel.objects.filter(mat_tale)

### Les terminales technologiques
mat_tale_techno = mat_1ere_techno_filiere & mat_tale_niveau
# sélection des manuels pouvant convenir à une terminale technologique :
# Materiel.objects.filter(mat_tale_techno)

