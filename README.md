# SLM: School Library Management

- **Other languages**: [Français](LISEZMOI.md), [Español](LEAME.md)
- **User manual, screenshots**: [see the contextual help of SLM](https://slm-demo.freeduc.org/aide/index.html) (currently in French only)
- **Demonstration**: send an e-mail to <georgesk@debian.org > with a subject "try SLM", to get access to a test server.

-----------------------------------------

<img alt="salam : SML" src="Collaboration_logo_V2.svg" style="width: 150px; float: left; margin-right: 12px;">

This project was started in year 2023, in Dunkirk's Jean Bart high school
(France). School books are managed there by a non-profit association,
the « coopérative du lycée », animated by volunteers. Students are
about one thousand five hundreds to enjoy hiring school books managed
by the <i>coopérative</i>; all those « clients » come within one week or less
when the school year begins, and come again at the end of the year to
give back the books they hired.

## Short history

<img alt="livre - Gemasco" src="book-gemasco.png"  style="width: 150px; float: right; margin-left: 12px;">
<b>SLM</b> owes a lot to <b>[GeMaSco](https://openacademie.fr/gemasco/)</b>,
a software to manage school books developed by the organization
[OpenAcademie](https://openacademie.fr/). To name the project
<b>SLM</b>, one can say « Salam » (سَّلَام, salām),
« Shalom » (שָׁלוֹם), which means « salute » or « hi! »,
but can also mean « peace ». Gemasco is based on
[Microsoft Access](https://fr.wikipedia.org/wiki/Microsoft_Access), 
it is a set of routines written in Visual Basic, interacting with
seventeen tables which contain the application's data.

## Migration GeMaSco → SLM

<img alt="Django-project logo" src="logo-django.svg" style="width: 150px; float: left; margin-right: 12px;">
A migration of the tables (which contained the series of books hired
during September 2023) was achieved, by rewriting their structures as
<i>classes</i> inheriting methods and properties from the
[Django](https://docs.djangoproject.com/fr/4.1/) framework, and then
importing the data, to populate a SQL database. After this migration,
it was possible to define <i>views</i> which provide <i>web pages</i>
based on <i>templates</i> and styling rules (form and content are separated).
One part of the interaction between web pages and their users is implemented
by Javascript programs, who take benefit from [jQuery](https://jquery.com/) and
[jQuery-UI](https://jqueryui.com/) libraries.

