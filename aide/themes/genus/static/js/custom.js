/* scripts spécifiques à l'aide de SLM */

var sommaireTimer = -1; /* une place pour accèder au timer qui replie le sommaire */

function restartSommaireTimer(){
    if (sommaireTimer > 0) clearTimeout(sommaireTimer);
    sommaireTimer = setTimeout(replieSommaire, 3000);
}

function replieSommaire(){
    $(".summary .collapsible").hide(500);
    $(".translation-header .collapsible").hide(500);
}

function deplieSommaire(){
    $(".summary .collapsible").show(100);
    $(".translation-header .collapsible").show(100);
    restartSommaireTimer();
}

function retourAuSommaire(){
    deplieSommaire();
    location.hash='#begin';
}

