---
Title: Auto-Suscripción
slug: auto_inscription
layout: page
date: 2023-06-24
lang: es-es
translated: true
---

El programa SLM permite la suscripción (de alumnos que no serían ya importados
desde una base de datos extraña (por supuesto SIÈCLE).

Las páginas vinculadas con la auto-suscripción son accesibles a todos
(incluso los miembros des equipo), excepto la página donde se crea una
invitación: **la autorización provisional dura 30 minutos** para
gestionar la auto-suscripción. Durante estos 30 minutos, se debe entrar los
datos des nuevo alumno, y después, volviendo a la página de invitación,
un miembro del equipo debe verificar y validar los datos.

En caso que los nuevos datos no sean entrados (por cualquiera persona),
luego validados durante los 30 minutos, la autorización desaparece, y
si un alumno quiere hacer su auto-suscripción, se debe empezar del inicio,
preguntando una nueva invitación.

## "La página de invitación"<a name="invitation"></a>

![el código-QR ]({static}../images/es-es/auto_inscription1.jpg){.shadowed}

La captura de pantalla por arribas presenta un código-QR, distinto
cada vez que se abre la página de auto-suscripción.

Si el alumno que quiere suscribir utiliza su móvil, este código-QR le
permite gestionarlo con su móvil, a la condición que el móvil accede
a la red Internet. Nota bene: no se podría utilizar el código-QR de la
captura de pantalla por arriba, porque ¡fue creado más de 30 minutos
en el pasado!

Se puede hacer también lo mismo desde el computador: por eso, se hace clic
en el enlace que está por debajo del código-QR.

## "La auto-suscripción" <a name="inscription"></a>

El alumno (o el miembro del equipo) entra datos en el formulario visible
por debajo, pues valida.

![gracias]({static}../images/es-es/auto_inscription2.jpg){.shadowed}

Después de validar, si todo pasa bien, se abre una página de gracias:

![formulario]({static}../images/es-es/auto_inscription3.jpg){.shadowed" style="width: 500px;}

Un enlace permite de volver à la entrada de datos con el formulario, en caso
de duda. Cuando esta página aparece, el alumno debe llamar un miembro del
equipo, que pasará por "Parámetros", y luego "Suscripción provisional".

## Verificación de los datos <a name="verification"></a>

La página de verificación de datos es accesible solamente para miembros
del equipo: allá se ven todas la suscripciones provisionales, existentes
desde menos de 30 minutos (contados desde el inicio de la suscripción).

![volviendo a la auto-suscripción]({static}../images/es-es/auto_inscription4.jpg){.shadowed}

Haciendo clic en el botón de suscripción provisional, aparece una página
que permite de verificar los datos previamente entrados, y corregirlos
eventualmente.

![verificación]({static}../images/es-es/auto_inscription5.jpg){.shadowed}

**ATENCIÓN**: es imprescindible pasar por la página de verificación, mismo
cuando el alumno hizo ningún error, ciertamente. Cuando la verificación
no es hecha por une miembro del equipo bastante pronto, los datos entrados
el alumno serán borrados de la base de datos.


