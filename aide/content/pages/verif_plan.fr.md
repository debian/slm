---
Title: Plan de salle
slug: verif_plan
layout: page
date: 2024-06-19
lang: fr-fr
---

Cette page permet de vérifier le ou les plans de la salle de stockage des
livres. Ces plans sont au format SVG, et certains rectangles doivent faire
partie de groupes identifiables :

<dl>
<dt>les étagères :</dt>
<dd>
il faut que dans le même groupe on trouve
un texte commençant par <tt>classes:</tt> ... par exemple, 
« classes: Secondes » : ce qui peut signifier que l'étagère sur ce plan
contiendrait des livres pour les élèves de seconde.
</dd>
<dt>les piles de livres :</dt>
<dd>Ces groupes doivent avoir un attribut
<tt>class="livres"</tt> ; pour définir ce genre d'attribut, on peut
travailler avec le logiciel <b>Inkscape</b> et éditer le groupe en mode
source SVG (raccourci-clavier = Maj+Ctrl+X) ; de plus chacun de ces
groupes doit contenir un élément <tt>circle</tt> ou <tt>ellipse</tt>
positionné de telle façon qu'il représente l'emplacement d'une étiquette
qui permettra de reconnaître la pile de livres dans l'étagère. Les piles de
livres doivent être incluses chacune dans une étagère identifiée.
</dd>
</dl>

## Choisir le plan <a name="choix"></a>
Quand on ouvre la page web sans aucun paramètre, il faut sélectionner 
un fichier SVG parmi ceux qui sont disponibles :


![sans paramètres]({static}../images/verif_plan1.png){.shadowed}

## Vérifier le plan<a name="verif"></a>

Une fois que le plan a été sélectionné, celui-ci s'affiche dans le bas 
de la page :


![avec paramètres]({static}../images/verif_plan2.jpg){.shadowed}

- le lien « <a>Choix d'une autre salle</a> » permet de revenir à la sélection.
- le bouton <button>Vérification des piles de livres</button> lance un programme qui
  parcourt tous les groupes de classe "livres", et peuple la partie en haut
  à droite de la fenêtre de boutons sur lesquels on peut cliquer pour
  « allumer » les voyants de signalisation des piles de livres. Ce même
  programme tente de valider que toutes les piles de livres sont pourvues
  d'abrégés corrects, et que tous les abrégés existants de la base de données
  ont été placés sur le plan.
- le bouton <button>Vérification des étagères</button> lance un autre programme
  qui donne une ligne de renseignements pour chaque étagère identifiée,
  affiche un bouton à survoler pour localiser l'étagère et affiche une
  liste d'abrégés de piles de livres posés sur cette étagère. Ce programme
  essaie aussi de valider que toutes piles de livres reposent bien sur 
  une étagère du plan.
  
### Vérification des piles de livres<a name="verif_piles"></a>

Dans la copie d'écran ci-dessous, on voit ce qui se passe après un appui
sur le bouton <button>Vérification des piles de livres</button> : 

- une liste de boutons violets apparaît en haut à droite, chacun pour un 
  des abrégés utilisés de piles de livres. Le bouton « allemand » a été
  cliqué et des « voyants » rouges apparaissent sur le plan là où des piles
  de livres sont entreposées.
- les cases de vérifications sont :
    - « Tous les abrégés sont corrects » 
      <input type="checkbox" checked onclick="return false"/> : la case est
	  cochée, ce qui signifie que les abrégés du plan sont bien des abrégés
	  définis dans la base de donnée aussi.
    - « Tous les abrégés sont présents en rayons » 
      <input type="checkbox" onclick="return false"/> : non cochée, en effet
	  certains abrégés définis dans la base de données ne sont pas présents sur
	  le plan ; la liste de ces abrégés définis dans le catalogue mais absents
	  du plan est écrite à droite, en dessous des boutons violets.
	
![avec paramètres]({static}../images/verif_plan3.jpg){.shadowed}

### Vérification des étagères<a name="verif_etageres"></a>

Dans la copie d'écran ci-dessous, on voit ce qui se passe après un appui
sur le bouton <button>Vérification des étagères</button> :

- une liste de catégories d'étagères apparaît : secondes, terminales,
  premières, terminales et premières technologiques. À chaque ligne de
  cette liste, on voit un ou plusieurs boutons beiges ; par exemple, pour
  les terminales non technologiques, le bouton est étiqueté « g1200 »
  (c'est l'identifiant du groupe qui représente cette étagère dans le code
  SVG), et comme on survole ce bouton « g1200 », l'étagère en bas à droite
  apparaît avec une auréole violette, et on peut la localiser. Juste après le 
  bouton « g1200 », il y a un champ où sont listés les abrégés des piles
  qu'il est possible de trouver sur l'étagère des terminales.
- la case « Chaque pile de livres est dans une étagère identifiée »
  <input type="checkbox" checked onclick="return false"/> est cochée, ce qui
  signifie qu'aucune pile de livres ne se trouve en dehors d'une étagère
  identifiée.
  
![avec paramètres]({static}../images/verif_plan4.jpg){.shadowed}

## Annexe : le fichier SVG utilisé pour l'exemple<a name="annexe"></a>

<a href="{static}../images/plan_salle_e01_2.svg" target="_new">
![le plan au format SVG]({static}../images/plan_salle_e01_2.svg){.shadowed title="Cliquer pour ouvrir dans un nouvel onglet et zoomer facilement, ou enregistrer le fichier"}
</a>
