---
Title: Sommaire de l'aide
slug: landing-page-about-hidden
layout: page
date: 2023-04-11
status: hidden
lang: fr
---

- [Le logiciel SLM](pages/slm/index.html)
- [La page d'accueil](pages/page-accueil/index.html)
- [L'authentification](pages/pages-autres-authentification/index.html), pour les autres pages
- [Paramètres](pages/parametres/index.html)
- [Achats/Inventaire](pages/achats_inventaire/index.html)
- [Prêts](pages/prets/index.html)
- [Retours](pages/retours/index.html)
- [Assistance](pages/assistance/index.html)
