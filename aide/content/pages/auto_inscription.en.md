---
Title: Auto-Subscription
slug: auto_inscription
layout: page
date: 2023-06-24
lang: en-us
translated: true
---

The application SLM features the subscription of students which were not
subscribed previously by importing an external database like SIÈCLE.

Pages about auto-subscription can be accessed by anybody (including team
members), except the one page which creates an invitation, which provides
**a temporary authorization during 30 minutes**, to deal with the
auto-subscription. During those 30 minutes, the data of the new student must
be entered, and when one team member comes back to the invitation page,
they can proof read and validate the data.

If the new data is not entered 'by anybody) and validated before the
30 minutes deadline, then the authorization disappears, and if a student
must make an auto-subscription, one must begin from scratch, by creating
a new invitation.

## "The invitation page" <a name="invitation"></a>

![the qr-code]({static}../images/auto_inscription1.jpg){.shadowed}

The screenshot above displays a QR-code, which is renewed each time this
page is reloaded.

If the student wanting to subscribe uses a mobile phone, this QR-code
allow them to manage the subscription with the phone, provided an access
to Internet. Please notice that one cannot use the QR-code above, as its
was issued more than 30 minutes in the past!

One can do the same also from the computer: to do so,
one clicks on the link placed just below the QR-code.

## "The auto-subscription" <a name="inscription"></a>

The student (o the team member) fills the form below, then validates it.

![the form]({static}../images/auto_inscription2.jpg){.shadowed}

After the validation, if everything went well, one gets the thanks page:

![thank you]({static}../images/auto_inscription3.jpg){.shadowed" style="width: 500px;}

A link allows one to go back and fill the form, in case of doubt.
When this page appears, the student has to call a team member, who will
click on "Parameters", then "Temporary subscription".

## Prof reading the data <a name="verification"></a>

The page to proof-read the data can be accessed only by team members:
all temporary subscriptions are visible there, when they are aged by
less than 30 minutes (counted from the start of the invitation).

![back to auto-subscription]({static}../images/auto_inscription4.jpg){.shadowed}

When one clicks on the button of a temporary inscription, one gets a page
where data previously entered can be controlled, and eventually corrected.

![verification]({static}../images/auto_inscription5.jpg){.shadowed}

**WARNING**: one must go to the verification page in any case, even
when the student has entered quite reliable data. If no member of the team
validates the data on due time, the data entered by the student will be
erased from the database.

