---
Title: Achats/Inventaire
slug: achats_inventaire
layout: page
date: 2023-04-12
lang: fr-fr
---

Le deuxième mène à la page « Achats/Inventaire »

![paramètres]({static}../images/achats_inventaire.jpg){.shadowed}

Plusieurs fonctions sont accessibles, à partir de là :

## « Inventaire » <a name="inventaire"></a>

![paramètres]({static}../images/inventaire0.jpg){.shadowed}

Cette fonction permet de rechercher :

- à qui a été prêté un livre, ou si ce livre est censé rester dans le stock ;
- d'obtenir les listes de livres prêtés ou non, sélectionnable par leur titre ;
- d'obtenir les listes d'élèves ayant reçu des livres ou non, sélectionnable par leur classe ;

### Recherche pour un livre<a name="recherche_livre"></a>

On inscrit (ou on flashe) le code du livre en haut de la page, et son 
statut s'affiche.

![paramètres]({static}../images/inventaire1.jpg){.shadowed}

### Livres non prêtés<a name="livres_nonpretes"></a>

Cette fonction fait partie des « accordéons » qu'on déplier ou replier.
Quand on clique sur la barre grise, celle-ci devient bleue et une liste des
livres non prêtés s'affiche ; les quarante premiers d'abord, puis plus si
on joue avec l'ascenseur à droite de l'affichage.

Il est possible de préciser un titre de livre : taper trois lettres, et un
choix des titres correspondants s'affiche. Quand on clique dans le menu 
déroulant sous le champ de saisie, le titre est sélectionner, et la liste est recalculée.

**N. B.** : si on clique sur un des numéros de livre présent dans l'afficheur,
son statut s'affiche aussitôt dans le haut de la fenêtre, sous le titre
*À qui est ce livre ?*

![paramètres]({static}../images/inventaire2.jpg){.shadowed}

### Livres prêtés à quelqu'un<a name="livres_pretes"></a>


Ce deuxième « accordéon » fonctionne à l'identique du précédent, mais 
concerne des livres prêtés à quelqu'un, comme annoncé dans le titre de la 
barre.

### Élèves avec au moins un livre prêté<a name="eleves_pretes"></a>

Le troisième accordéon permet une recherche d'élèves ; le champ de saisie
au-dessus de l'afficheur permet de préciser une classe, et il suffit de 
taper deux lettres, pour sélectionner une classe parmi le menu déroulant qui
apparaît alors.

**N. B.** : si on clique sur un des noms d'élèves présent dans l'afficheur,
la fenêtre des prêts de livres s'ouvre aussitôt, pour l'élève concerné.

![paramètres]({static}../images/inventaire3.jpg){.shadowed}

### Élèves sans aucun livre prêté<a name="eleves_nonpretes"></a>

Le quatrième accordéon fonctionne comme le troisième, à ceci près
que la recherche concerne des élèves qui n'empruntent couramment aucun livre.

## « Catalogue » <a name="catalogue"></a>

Cette fonction donne accès au catalogue des livres gérés par SLM.

![paramètres]({static}../images/catalogue0.jpg){.shadowed}

Pour chacun des titres de livres référencés dans la base, il est possible de :

- ![paramètres]({static}../images/catalogue1.jpg){.shadowed} supprimer le titre
- ![paramètres]({static}../images/catalogue2.jpg){.shadowed} modifier la fiche qui concerne ce titre
- ![paramètres]({static}../images/catalogue3.jpg){.shadowed} cloner la fiche qui concerne ce titre, pour saisir par exemple une nouvelle édition 
-  ![paramètres]({static}../images/catalogue4.jpg){.shadowed} ajouter des livres de ce titre à l'inventaire des manuels, en créant les étiquettes à code-barre correspondantes
- imprimer la liste des titres gérés par SLM.
- enregistrer la fiche d'un nouveau titre

Pour les deux dernières fonctions, ce sont les boutons plus gros 
en haut à gauche :

![paramètres]({static}../images/catalogue5.jpg){.shadowed}

## Ajout de manuels à l'inventaire et gestion des codes-barres <a name="ajout_codes"></a>

![paramètres]({static}../images/codes_barres.jpg){.shadowed}

Cette fonction permet de voir les codes-barres générés précédemment et de
les réimprimer s'il le faut, sinon on peut aussi créer de nouvelles étiquettes
à codes-barres. Dès que les nouveaux codes-barres sont générés, autant
d'enregistrements de manuels neufs sont ajoutés à l'inventaire, et il
est possible aussitôt de créer les pages d'étiquettes à l'imprimante.

## Impression de codes-barres <a name="impression_codes"></a>

Cette fonction est fille de la page web 
[« Catalogue »](#catalogue) ...

Quand on cliqué sur un des boutons actifs, à l'étape précédente 
(voir ci-dessus), on accède à la page de prévisualisation des
planches de codes-barres à imprimer.

![paramètres]({static}../images/impression_codes_barres.jpg){.shadowed}

Quand on en est à imprimer les codes-barres, on passe par une étape de
« prévisualisation », qui permet de personnaliser l'emplacement où les
étiquettes seront imprimées (sur une planche d'étiquettes 3 x 8, de 
référence "Avery J8159"). *Attention* : seule la première page a des
positions non-imprimées ; pour les pages suivantes, l'impression des
étiquettes se poursuit sans laisser d'espace vide. Bien sûr, quand on 
arrive à la dernière page à imprimer, certaines zones à la fin peuvent
rester non-imprimées.

**N. B.** : quand on finit par cliquer sur le bouton d'imprimante, c'est
un fichier PDF qui est créé, on a besoin ensuite d'ouvrir ce fichier PDF 
afin de l'imprimer. La plupart des navigateurs permettent d'automatiser
l'ouverture du fichier PDF téléchargé.

## « Tarifs » <a name="tarifs"></a>

Cette fonction n'est pas encore implémentée, pour SLM version 1.2.0

Elle permet de mettre à jour des systèmes tarifaire applicables quand un
livre a été perdu ou dégradé.

## « Bon de commande » <a name="bon_commande"></a>

Cette fonction n'est pas encore implémentée, pour SLM version 1.2.0

Elle permet d'élaborer le bon de commande pour acquérir des manuels.

