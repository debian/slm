---
Title: Compras/inventario
slug: achats_inventaire
layout: page
date: 2023-04-12
lang: es-es
translated: true
---

La secunda pestaña dirige hasta la página "Compras/inventario"

![parámetros]({static}../images/es-es/achats_inventaire.jpg){.shadowed}

Allá, algunas funciones son accesibles:

## Inventario" <a name="inventaire"></a>

![parámetros]({static}../images/es-es/inventaire0.jpg){.shadowed}

Esta función permite de buscar:

- a quien fue prestado un libro, o si este libro permanece en el almacén;
- de ver las listas de libros prestados o no, posiblemente selectos por su título;
- de ver las listas de alumnos que tienen libros o no, posiblemente selectas por su clase;

### Buscar por un libro <a name="recherche_livre"></a>

Se teclea (o se flashea) el código del libro en parte arriba de la página,
y su estatuto aparece.

![parámetros]({static}../images/es-es/inventaire1.jpg){.shadowed}

### Libros no prestados <a name="livres_nonpretes"></a>

Esta función es parte de los "acordeones"que pueden ser desplegados o
replegados. Cuando se hace clic en la barra gris, esta se vuelve azul
y una lista de libros no prestados aparece; los cuarenta primeros al
inicio, y luego más si se utiliza el ascensor a la derecha de la ventana.

Se puede precisar un titulo de libro: teclear tres letras, y escoger uno de
los títulos propuestos cuando aparecen. Cuando se hace clic en el menú
aparecido debajo del campo de entrada, el titulo es seleccionado, y la lista
es computada del inicio.

**N. B.**: si se hace clic en un de los números de libros presente en
esta parte de la ventana, su estatuto aparece inmediatamente en la parte
arriba de la ventana, sobre el título "¿De quién es este libro?"

![parámetros]({static}../images/es-es/inventaire2.jpg){.shadowed}

### Libros prestados a alguien <a name="livres_pretes"></a>

Este segundo "acordeón" funciona como el previo, pero es para libros prestados
a alguien, como escrito en el título de la barra.

### Alumnos que tienen al menos un libro <a name="eleves_pretes"></a>

El tercer acordeón permite de buscar alumnos; el campo de entrada encima de
esta parte de la ventana permite de precisar una clase, y es bastante con
teclear dos letras, luego seleccionar una clase en el menú que aparece.

**N. B.**: si se hace clic en un nombre de alumno apareciendo en esta parte de
la ventana, la página de préstamos de libros se abre, con este alumno.

![parámetros]({static}../images/es-es/inventaire3.jpg){.shadowed}

### Alumnos sin libro prestado <a name="eleves_nonpretes"></a>

El cuarto acordeón funciona como el tercero, con la diferencia que
aparecen alumnos corrientemente sin libro prestado.

## "Catálogo" <a name="catalogue"></a>

Esta función dirige hasta el catálogo de libros gestionados por SLM.

![parámetros]({static}../images/es-es/catalogue0.jpg){.shadowed}

Para cada título de libros conocidos en la base de datos, es posible de:

- ![parámetros]({static}../images/catalogue1.jpg){.shadowed} borrar el título
- ![parámetros]({static}../images/catalogue2.jpg){.shadowed} editar los datos de este libro
- ![parámetros]({static}../images/catalogue3.jpg){.shadowed} clonar el registro de este título, para entrar por ejemplo una nueva edición
-  ![parámetros]({static}../images/catalogue4.jpg){.shadowed} añadir libros con este título en el inventario de libros escolares, y crear etiquetas con códigos-barra
- imprimir la liste de títulos gestionados por SLM.
- registrar los datos de un nuevo título

Para las dos ultimas funciones, se utiliza los botones más largos en la parte
arriba izquierda:

![parámetros]({static}../images/catalogue5.jpg){.shadowed}

## Añadir libros escolares en el inventario y gestionar sus códigos-barra <a name="ajout_codes"></a>

![parámetros]({static}../images/es-es/codes_barres.jpg){.shadowed}

Esta función permite de ver los códigos-barra generados previamente y
volver à imprimirlos si está necesario, o también crear nuevas etiquetas
con código-barra. Cuando códigos-barra son generados, tantos registros de
libros nuevos son añadidos en el inventario, y es posible de crear hojas
de etiquetas con la impresora.

## Imprimir códigos-barra <a name="impression_codes"></a>

Esta función es hija de la página web
["Catalogo"](#catalogue) ...

Cuando se ha hecho clic en un botón activos, en la etapa previa
(véase por arriba), aparece una página de vista previa de
hojas de códigos-barra para imprimir.

![parámetros]({static}../images/es-es/impression_codes_barres.jpg){.shadowed}

En este contexto, hay una etapa de "vista previa", que permite de arreglar
las plazas donde serán imprimidas las etiquetas (en una hoja organizada
en  x 8, de referencia "Avery J8159"). *Atención*: únicamente la primera
hojas puede tener zonas sin impresión; par las hojas siguientes, la impresión
de etiquetas sigue si espacio vació. De modo evidente, cuando viene la
ultima hoja, algunas zonas finales quedan sin impresión.

**N. B.**: cuando se hace clic finalmente el botón de impresión, un
archivo PDF es generado, y se abre este archivo PDF par imprimirlo.
Casi todos los navegadores permiten de automatizar la gestión del archivo
PDF descargado.

## "Tarifas" <a name="tarifs"></a>

Esta función no está implementada ya, en SLM versión 1.2.0

Esta permite de gestionar sistemas de tarifas aplicables cuando un libro
es perdido o degradado.

## "Órdenes de compra" <a name="bon_commande"></a>

Esta función no está implementada ya, en SLM versión 1.2.0

Esta permite de elaborar órdenes de compra para los libros.

