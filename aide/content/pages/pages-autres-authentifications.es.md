---
Title: Autenticación
slug: pages-autres-authentification
layout: page
date: 2023-04-11
lang: es-es
translated: true
---

Cuando se abre una página web otra que la página principal,
una pregunta de autenticación aparece:

![pregunta de autenticación]({static}../images/es-es/authentification1.jpg){.shadowed}

Entonces, se hace clic en el enlace "por favor autenticar"

![autenticación]({static}../images/es-es/authentification2.jpg){.shadowed}

La captura de pantalla por arriba presenta la página de autenticación:
se puede disfrutar de las funciones del navegador para memorizar
las contraseñas, si su máquina es segura. Cuando la autenticación es hecha,
otras páginas son accesibles.

**N.B.**: hay dos categorías de usuarios,

1. los usuarios del equipo: pueden acedar à todas la funciones, con la
   excepción de la administración directa de la base de datos;
2. los súper-usuarios: aquellos aceden a todas la funciones, incluida
   la administración directa de la base de datos; esta posibilidad
   implica responsabilidades, porque una maniobra irreflexiva puede
   dañar fácilmente la base de datos.
