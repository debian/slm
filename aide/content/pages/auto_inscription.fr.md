---
Title: Auto-Inscription
slug: auto_inscription
layout: page
date: 2023-06-24
lang: fr-fr
---

L'application SLM autorise l'inscription d'élèves qui n'ont pas encore
été importés depuis la base SIÈCLE.

Les pages liées à l'auto-inscription sont accessibles à n'importe qui
(y compris aux membres de l'équipe), sauf la page qui crée une invitation, qui
crée une **autorisation temporaire de 30 minutes** pour s'occuper de
l'auto-inscription. Durant ces 30 minutes, il faut saisir les données du
nouvel élève, puis, en revenant à la page de l'invitation, un membre de
l'équipe doit relire et valider les données.

Si les nouvelles données ne sont pas saisies (par n'importe qui), puis
validées durant les 30 minutes, alors l'autorisation disparaît, et si un élève
veut faire son auto-inscription, il doit recommencer, en demandant une nouvelle
invitation.

## « La page d'invitation »<a name="invitation"></a>

![le qr-code]({static}../images/auto_inscription1.jpg){.shadowed}

La copie d'écran ci-dessus présente un QR-code, qui est renouvelé à
chaque fois qu'on affiche la page d'auto-inscription.

Si l'élève qui veut s'inscrire utilise un téléphone, ce QR-code lui permet de
s'en occuper à partir du téléphone, à condition que le téléphone accède
au réseau Internet. Notez bien qu'on ne pourrait pas utiliser le QR-code
de cette copie d'écran, car son ancienneté remonte à plus de 30 minutes !

Il est aussi possible de faire la même chose sur l'ordinateur : pour cela,
on clique sur le lien qui se trouve en dessous du QR-code.

## « L'auto-inscription »<a name="inscription"></a>
L'élève (ou le membre de l'équipe) remplit le formulaire ci-dessous,
puis valide.

![merci]({static}../images/auto_inscription2.jpg){.shadowed}

Après validation, si tout se passe bien, on obtient une page de remerciement :

![formulaire]({static}../images/auto_inscription3.jpg){.shadowed" style="width: 500px;}

Un lien permet de retourner à la saisie du formulaire, en cas d'hésitation.
Quand cette page apparaît, l'élève doit appeler un membre de l'équipe, qui
passera par « Les paramètres », puis « Inscription provisoire »

## Vérification des données<a name="verification"></a>

La page pour vérifier les données n'est accessibles qu'aux membres de
l'équipe : on y voit toutes les inscriptions provisoires, datant de moins de
30 minutes (après le début de l'inscription).

![retour à l'auto-inscription]({static}../images/auto_inscription4.jpg){.shadowed}

En cliquant sur le bouton de l'inscription provisoire, on arrive à une page
qui permet de contrôler la saisie faite précédemment, et de faire
éventuellement des corrections.

![verification]({static}../images/auto_inscription5.jpg){.shadowed}

**ATTENTION** : il faut toujours passer par la page de vérification, même
s'il est certain que l'élève n'a pas fait d'erreur. Si aucun
membre de l'équipe ne valide la vérification assez vite, la saisie faite par
l'élève sera effacées de la base de données.

