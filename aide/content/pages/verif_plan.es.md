---
Title: Mapa_de_aula
slug: verif_plan
layout: page
date: 2024-06-19
lang: es-es
translated: true
---

Esta página es para verificar el mapa (o los mapas) de la aula donde se colocan
libros de la cooperativa. Estos mapas son en formato SVG, y algunos rectángulos
particulares deben pertenecer a grupos identificables:

<dl>
<dt>los estantes:</dt>
<dd>
en el mismo grupo que el rectángulo se debe encontrar un texto empezando
por <tt>classes:</tt> ... por ejemplo,
« classes: Secondes » : significa que este estante del mapa puede contenga
libros para alumnos de "seconde".
</dd>
<dt>pilas de libros:</dt>
<dd>Estos grupos deben tener un atributo
<tt>class="livres"</tt>; para definir un tal atributo, se puede
utilizar el software <b>Inkscape</b> y editar el grupo en modo
"fuente SVG" (atajo de teclado = May+Ctrl+X); además, cualquier de estos
grupos debe contener un elemento <tt>circle</tt> o <tt>ellipse</tt>
situado de tal modo que represente una etiqueta para reconocer la pila
de libros en el estante. La pilas de libros deben ser incluidas totalmente
cada en un estante identificado.
</dd>
</dl>

## Escoger el mapa <a name="choix"></a>

Cuando se abre la página web sin parámetro, hay que seleccionar
un archivo SVG entre los que están disponibles:


![sin parámetro]({static}../images/es-es/verif_plan1.jpg){.shadowed}

## Verificar el mapa <a name="verif"></a>

Cuando el mapa fue seleccionado, aparece en la parte baja de la ventana:


![con parámetro]({static}../images/es-es/verif_plan2.jpg){.shadowed}

- el enlace "¿Elegir otra aula?" vuelve a la selección.
- el botón <button>Comprobación de pilas de libros</button> lanza un programa
  que examina todos los grupos con atributo <tt>class="livres"</tt>, y llena
  la parte arriba derecho de la ventana con botones que pueden se activados
  haciendo clic, para "encender" la "bombillas" que señalan pilas de libros.
  Este mismo programa intenta validar que todas la pilas de libros tienen
  abreviaturas correctas, y que todas la abreviaturas existentes en la base
  de datos fueron colocadas en el mapa.
- el botón <button>Comprobación de estantes</button> lanza un otro programa
  que añade una línea de información a cada estante identificado, mostra
  un botón para localizar el estante pasando el cursor sobre él y mostra
  una lista de abreviaturas de la pilas de libros incluidas en el estante. Este
  programa también intenta validar que cada pila de libros es correctamente
  incluida en un estante del mapa.
  
### Comprobación de pilas de libros <a name="verif_piles"></a>

En la captura de pantalla abajo, aparece lo que pasa después de hacer
clic en un botón <button>Comprobación de pilas de libros</button>:

- una lista de botones morados aparece en la parte arriba derecha, cada uno
  para una abreviatura utilizada de pila de libros. Se ha hecho clic en
  el botón "allemand" y "bombillas" rojas están "encendidas" en el mapa
  en posiciones de pilas de libros correspondientes.
- las casillas de verificación son:
    - "Todas las abreviaturas son correctas"
      <input type="checkbox" checked onclick="return false"/>: la casilla
      está checked, porque las abreviaturas del mapa pertenecen también
      a la base de datos.
    - "Todas las abreviaturas están presentes en los estantes"
      <input type="checkbox" onclick="return false"/> : la casilla no
      está checked, porque algunas abreviaturas faltan en el mapa; la lista de
      abreviaturas que existen el la base de datos, pero faltan en el mapa
      aparece en la derecha, debajo de los botones morados.
	
![con parámetro]({static}../images/es-es/verif_plan3.jpg){.shadowed}

### Comprobación de estantes <a name="verif_etageres"></a>

En la captura de pantalla abajo, aparece lo que pasa cuando se ha hecho
clic en el botón <button>Comprobación de estantes</button>:

- una lista de categorías de estantes aparece: "secondes", "terminales",
  "premières", "terminales et premières technologiques". En cada linea
  de esta lista aparece uno (o más) botón(es) de color beige; por ejemplo,
  para los "terminales non technologiques", el botón tiene la etiqueta "g1200"
  (es la identificación del grupo que representa este estante en el código
  SVG), y cuando se pasa el cursor sobre este botón "g1200", el estante en
  la parte baja derecha aparece con un nimbo morado, y puede ser localizado
  así. Inmediatamente después del botón "g1200", hay una zona donde están
  listadas la abreviaturas de pilas de libros que pueden ser encontradas
  en el estante de los "terminales".
- la casilla "Cada pila de libros está en un estante identificado"
  <input type="checkbox" checked onclick="return false"/> es checked,
  y significa que no hay pila de libros excluida de los estantes identificados.
  
![con parámetro]({static}../images/es-es/verif_plan4.jpg){.shadowed}

## Anexa : el archivo SVG utilizado en este ejemplo <a name="annexe"></a>

<a href="{static}../images/plan_salle_e01_2.svg" target="_new">
![el mapa en formato SVG]({static}../images/plan_salle_e01_2.svg){.shadowed title="Haga clic para abrir el archivo en una nueva pestaña, para manejar el zoom, o para registrarlo"}
</a>
