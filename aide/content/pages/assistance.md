---
Title: Assistance
slug: assistance
layout: page
date: 2023-04-12
lang: fr
---

Cette fonction rassemble quelques sous-fonctions qu'on ne savait pas mettre
ailleurs.


![Assistance]({static}../images/assistance0.jpg){.shadowed}

## « Aide » <a name="aide"></a>

Quand on clique sur le lien « Aide », on arrive à la page d'accueil
du présent manuel de l'utilisateur.

**N. B.** : l'étoile à cinq branches, qui est toujours présente à droite
de la fenêtre, et qui devient plus nette dès qu'on la survole avec le curseur
de souris, mène à une aide *contextuelle* ; si on clique cette étoile, dans
le contexte de la page qui correspond à la copie d'écran ci-dessus ... eh bien,
on en vient à lire cette ligne précisément.

## « Sauvegarde » <a name="sauvegarde"></a>

Il est bon de réaliser une sauvegarde de la base de données à chaque fois 
qu'un travail conséquent a été réalisé, par exemple à la fin d'une journée 
de travail.

![Sauvegarde]({static}../images/sauvegarde.jpg){.shadowed}

Pour le moment, seul le bouton « format Sqlite » permet une sauvegarde
fiable. On télécharge alors un fichier nommé par exemple
`db.sqlite-2023-04-12_17-03-45.zip`, qui est au format ZIP, et qui permet
d'obtenir, après décompression, un fichier nommé `db.sqlite3`. Ce fichier
contient la totalité de la base de données ; il permet à un administrateur
qui a des droits sur la machine qui fournit le service SLM, de restaurer
son fonctionnement exactement à ce qu'il était au moment de la
sauvegarde.

Avec l'exemple du fichier nommé `db.sqlite-2023-04-12_17-03-45.zip`, il est
donc possible de restaurer l'état de la base de données tel qu'il était le
12 avril 2023 à 17 heures 03 minutes et 45 secondes.

## « Maintenance » <a name="maintenance"></a>

Un utilisateur *ordinaire* de SLM, qui fait partie de l'équipe qui peut
s'authentifier, ne peut pas faire grand chose en cliquant sur 
« Maintenance » ; en effet, la maintenance de la base de donnée est réservée
aux utilisateurs qui ont des droits de « super-utilisateur ».

![pas de maintenance]({static}../images/admin0.jpg){.shadowed}

Si on est  utilisateur *ordinaire* de SLM, mais qu'on connaît une façon
d'être accrédité du droit de « super-utilisateur », alors on peut se
déconnecter de SLM, puis se réauthentifier avec l'accrédiation (nom
d'utilisateur et mot de passe) qui correspond à un super-utilisateur.

Voici à quoi ressemble l'écran quand on dispose de ce droit ; on peut
alors maintenir directement la base de données : effacer des données
arbitrairement, en ajouter de nouvelles, etc.

![maintenance]({static}../images/admin1.jpg){.shadowed}

**Il est fortement recommandé** de faire une **sauvegarde** de la base de
données avant d'entreprendre une quelconque maintenance. Les actions de
chaque super-utilisateur sont journalisées dans la base de données, du 
moins tant qu'on s'en tient à l'outil de maintenance propre au logiciel
Django, qui anime le service web SLM.

