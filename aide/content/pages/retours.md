---
Title: Retours
slug: retours
layout: page
date: 2023-12-01
lang: fr
---

Cette page est celle qui permet de gérer retours de manuels en fin 
d'année scolaire.

## « Retours » <a name="retours"></a>

Le fonctionnement du retour des livres est en grandes partie similaire à
celui du prêts des livres détaillé ci-dessus : on précise le nom de l'élève,
puis on tape ou on flashe les codes de livres rendus.

![retours]({static}../images/retours1.jpg){.shadowed}

À mesure qu'un renseigne des retours de livres, les lignes du tableau,
initialement en noir & blanc, deviennent colorées :

- en vert, par défaut, dès qu'un livre vient d'être saisi. 
- en orange, si après la saisie du livre, on modifie l'état final pour
  signaler une dégradation. La modification se fait en modifiant le choix
  dans le menu déroulant de la colonne « État final », à droite.
- si l'élève déclare un livre perdu, on coche la case de la colonne
  « Perdu » et dans ce cas, la ligne apparaît sur fond rouge.
  
**N. B.** : les changements apportés au tableau des retours restent 
accessibles et modifiables autant de fois que l'on veut, si on le fait
dans la même journée. Les repentir possibles sont : retirer la mention
« Perdu » pour une ligne, et annuler une ligne de retour 
(le bouton d'annulation est voisin de la date de retour).

## « Même classe » <a name="meme-classe"></a>

![élèves de la même classe]({static}../images/retours2.jpg){.shadowed}

À la droite de la page web, il y a un rappel de la liste d'élèves de la classe.
Cette liste d'élèves apporte quelques facilités :

  - chaque nom d'élève dans la liste porte un lien qui permet de prêter (ou de
    rendre) les livres de cet élève
  - la liste des « classe précédentes » où l'élève a été inscrit apparaît.

Dans le contexte des retours, le haut de la zone « même classe » contient deux
boutons : la flèche à gauche, pour aller directement à une page de prêts,
et le deuxième bouton sert à déclarer qu'on va effectivement rendre la caution.

