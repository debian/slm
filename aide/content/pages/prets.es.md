---
Title: Préstamos
slug: prets
layout: page
date: 2023-12-01
lang: es-es
translated: true
---

Esta página sirve para gestionar préstamos de libros a los alumnos
cuando empieza el año escolar.

## Préstamos <a name="prets"></a>

La función de préstamo permite tres cosas:

1. examinar la lista de libros ya prestados a un alumno,
2. añadir libros para prestarle-los,
3. imprimir la hoja de préstamo, que el alumno debe firmar.

![préstamos]({static}../images/es-es/prets1.jpg){.shadowed}

**Aquí está el modo de empleo:**

1. Tecla tres letras del apellido o del nombre del alumno:
   un menú desplegante aparece, donde puedes escoger un nombre completo
   par el alumno; cuando este nombre fue escogido, la lista de libros
   prestados a este alumno aparece, y el cursor se fija en el campo de entrada
   del número de libro; entonces puedes teclear este número, o flashear su
   código-barra con el lector óptico.
2. A cada entrada de un número de libro, este libro es añadido a la lista
   de nuevos libros a prestar, eventualmente después de una pregunta
   de confirmación (por ejemplo, cuando dos libros con el mismo título
   son prestados al mismo alumno).
3. Cuando la lista de libros a prestar es correcta, puedes hacer clic
   en el botón de registración, para insertar estos libros en la lista de
   "libros registrados". Después, puedes volver a añadir otros libros
   (¡pero no puedes anular los libros recién añadidos!).
4. Finalmente, haces clic en el botón de impresora y crear la hoja de
   préstamos. Esta es descargada al formato PDF. Si el navegador es bien
   arreglado, puedes leerla para verificación, y imprimirla.

En la etapa 2, se pueden anular libros, hasta el momento que la lista
sera validada.

La lista de préstamos contiene libros ya prestados: en consecuencia,
debes validar la lista de nuevos libros antes de imprimir la hoja de
préstamos.

## "Enmiendas" <a name="avenants"></a>

Puedes hacer una enmienda si un o algunos préstamos fueron añadidos,
mientras una hoja de préstamos antigua ya es firmada.

Si se añade nuevos libros después de libros prestados **en días anteriores**,
el programa detecto el caso y propone la impresión de una enmienda relativa
a una hoja de préstamo antigua. La enmiendo se imprime al revés de la hoja
ya firmada por el alumno.

![préstamos]({static}../images/es-es/prets2.jpg){.shadowed}

La captura de pantalla arriba presenta dos nuevos botones par imprimir
documentos, y la información al propósito del primero botón extra:
"Agregar una primera enmienda para 1 libro(s)". Este botón sirve para
colocar una enmienda en la parte arriba de una hoja, el botón por debajo
sirve cuando una enmienda ya fue imprimida anteriormente en la parte
arriba; en este caso, la nueva enmienda será colocada en la parte baja
de la hoja.

## "Misma clase" <a name="meme-classe"></a>

![alumnos de la misma clase]({static}../images/es-es/prets3.jpg){.shadowed}

En parte derecha de la página web, hay una lista de alumnos de la misma clase.
Esta lista facilita algunas operaciones:

  - cada nombre de alumno es un enlace para prestar(o devolver) libros de
    este alumno
  - la lista de "clases previas" donde el alumno fue suscrito aparece.

En el contexto de préstamos, hay botones de acción (como en la captura de
pantalla arriba) par imprimir una tarjeta de membresía, o para ir
inmediatamente a la devolución de libros para el mismo alumno.
