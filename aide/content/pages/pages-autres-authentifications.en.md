---
Title: Authentication
slug: pages-autres-authentification
layout: page
date: 2023-04-11
lang: en-us
translated: true
---

When one opens web pages other than the home page,
an authentication query appears:

![authentication query]({static}../images/authentification1.jpg){.shadowed}

One must then click on the link "please authenticate"

![authentication]({static}../images/authentification2.jpg){.shadowed}

the screenshot above presents the authentication page;
one can use the browser's features to memorize password, when
one is working on a secure enough computer. As soon as the authentication
is done, other pages become available.

**N.B.**: there are two kinds of users,

1. members of the team: they access all the features, except the
   direct administration of the database;
2. super-users: those can access all features, including direct
   administration of the database; this possibility entails responsibilities,
   because a wrong operation can easily damage the database.
