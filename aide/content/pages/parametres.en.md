---
Title: Parameters
slug: parametres
layout: page
date: 2023-04-11
lang: en-us
translated: true
---

The left tab leads to the "Parameters" page

![parameters]({static}../images/en-us/parametres1.png){.shadowed}

A few features are available there:

## "Students database" <a name="base_eleves"></a>

When data from an external database like SIÈCLE is imported, SLM has an
up-to-date database.
The feature "Students database" allows one to get the record of a student,
given one fragment of their name or surname, or given the class which the
student belongs to.

When a student comes and is unknown in the database, the case can happen
because they are new in the school, and that their record could not be
imported from an external database like SIÈCLE to SLM's database. In such a case
(which must be exceptional), one can invite the student to create themselves
a new record in the "Students database". To do so, one must go to
<a href="../auto_inscription/index.html">"Parameters → Temporary subscription"</a>

![Students database]({static}../images/en-us/eleves1.png){.shadowed}

The screenshot above is the page of "Students database" when nothing has
been entered so far.

As soon as three characters are type in the filed **Student**, a list of
choices appears...

<a name="suivi_individuel"></a>
![Record of a student]({static}../images/en-us/eleves2.png){.shadowed}

... and when a choice is selected and validated, the data of the student
appear, and can be modified when necessary.


![data of a student]({static}../images/en-us/eleves3.png){.shadowed}

If the name of a class is typed in the field **Class**, a list of students
appears;

![class of TG03]({static}../images/en-us/eleves4.png){.shadowed}

if one clicks on a student's line in the list, then their data appear, which
is much alike the previous case.

<a name="suivi_individuel"></a>
![fiche de l'élève]({static}../images/en-us/eleves3.png){.shadowed}

Here above is the record of a student. One can see the action button
![button of tracking sheet]({static}../images/en-us/print_fiche_suivi.png){.shadowed}
which can be used to print the tracking sheet for the current student.

**Warning**: if one wants to make a *change*, it is not saved immediately in
the tracking sheet; one must first save explicitly the changed data, then
reopen the student's record and finally print their tracking sheet.

## "Membership cards" <a name="cartes_membre"></a>

When one selects this feature, one is led to the page of membership
cards management; here below is a screen shot of this page, in its
initial state:

![Initial state of the page of cards]({static}../images/en-us/cartemembre1.png){.shadowed}

The printer button, at this moment, allows one to print **reverse pages**
of membership cards only. *One can come back to this state by going to
the Parameters pages, and back, if necessary*.

A little underneath, one can select a set of classes.

  - a left click on a class selects that one and deselects all others;
  - a Ctrl+Left click on a class toggles its selection state without
    changing other states;
  - with a click and drag operation, one can select a big group of classes
    in a single move.

As soon as one or more classes are selected, the print button can be used to
print membership cards (not the reverse page; the cards will be pre-filled
with the record of a student). Two screenshots are displayed below, one
*during* the click-an-drag, the other one *immediately after* it: the
group of classes "premières G" is selected.


![selection is going on]({static}../images/en-us/cartemembre2.png){.shadowed}

![selection is finished]({static}../images/en-us/cartemembre3.png){.shadowed}

### "Membership cards", when some classes are selected <a name="cartes_membre_selection"></a>

When some classes are selected, the print button allows on to access another
dialog, to precise the information about cards already removed from the
printable tearable sheet: so, if a count between two and five cards are
still remaining in a tearable sheet, this can be managed to prevent paper waste.

![layout of the first page]({static}../images/en-us/cartemembre4.png){.shadowed}

The screenshot above displays a few areas:

  - the list of classes for which membership cards will be printed;
  - the number of sheets to be printed;
  - the possibility to manage the layout of the first page;
    for example here, the number of cards to print is 3 -- this number
    can be adjusted between 2 and 6 cards. A click on the button
    "Preview" updates the layout of cards to be printed.

The print button is to download the membership cards sheets, with the
layout of the first page driven by the current settings.

## "Temporary subscription" <a name="inscription"></a>

The procedure of temporary subscription must be used only for exceptions,
when a student comes but their record was not yet imported from an external
database like SIÈCLE.

<a href="../auto_inscription/index.html">Click here</a> for a more
detailed help page.

## "Deposits" <a name="cautions"></a>

There can be managed the deposit records of students:

  - print more pages of the book of deposits
  - manage the deposit of a student which exists in the database

Each time when a new student is imported from an external database like
SIÈCLE, a new deposit number is created and attached to their record.
This deposit number appears on the annual tracking sheets, and on membership
cards. Records in the book of deposits are never erased, even when a student
is erased from SLM's database, for example when they leave the school after
giving back all of their books.

Every deposit record has a creation date, and an end date (which become valid
after the deposit has been given back). On can define deposits with a zero
value, which mean that the students have the status "no deposit"; in such a
case the end date has less meaning.

<a href="../caution/index.html">Click here</a> for a more detailed
help page.

## "Constants" <a name="constantes"></a>

This page has three sections: "Constants of the school",
"Predefined messages", and "Print tracking sheets"

### Constants of the school <a name="contantes_etab"></a>

It is about definitions like the name of the school, its address, names
of managers, and so on.

Buttons, which are grouped by three as that:
![buttons]({static}../images/en-us/constantes1.png){.shadowed}
feature respectively:

  1. Erase the record of the current line;
  2. Edit the record of the current line;
  3. Clone the record of the current line and edit it.

After the lines of constants, the bigger button
![button plus]({static}../images/en-us/constantes2.png){.shadowed}
allows one to create an empty record and edit it from scratch.

### Predefined messages <a name="messages"></a>

There are defined the messages which will appear in some printable
documents featured by SLM: for example sheets for book loans.

There are analogous action buttons, with the same roles as explained
previously.

### Print the tracking sheets <a name="fiches_suivi"></a>

Finally, the third part of the page allows one to print the tracking sheets
of students. The action buttons
![button of tracking sheet]({static}../images/en-us/constantes3.png){.shadowed}
allow one to generate the PDF file of tracking sheets; the first one,
the biggest, allows one to generate all of the tracking sheets (typically
many hundreds); other buttons, which are smaller, allow one to print the
tracking sheets for one class (typically thirty sheets).

When one only wants to print just a few tracking sheets, for a few students,
one rather goes to
<a href="#suivi_individuel">"Students database"</a>.

## "Purge old loans" <a name="purger"></a>

Laws like the French *Informatique et libertés* rule that data about persons
must be erased after some time; this applies to data about book loans, and
that is useful when a book has been lent then given back, after some time it
is no longer useful to keep the history of each student who had it.

This function allows one to select the loans-given-back, i. e.
operation whose date of begin and date of end are known. When a range of
dates is defined, one can see how much operations can be "forgotten".
The deletion is definitive in the current database; however older
backups of this database might be done.

## "Import SIÈCLE" <a name="siecle"></a>

SIÈCLE is the name of a database currently used by French schools. This
function is for updating students in SLM's database.

To import a SIÈCLE file, one has to export it in CSV format, with UTF-8
encoding. This work is rather done by a super-user able to manage
SLM's database, as this feature is in development, from version 1.2 of SLM.



