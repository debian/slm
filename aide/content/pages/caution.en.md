---
Title: Deposits
slug: caution
layout: page
date: 2023-09-13
lang: en-us
translated: true
---

Here is the screenshot of the page of deposits:

![deposits]({static}../images/caution1.png){.shadowed}

The button with a printer icon allows one to download the last version
of the book of deposits in printable PDF format. This book changes only
by adding new lines, and new pages when necessary. So one can print and
keep pages of this book when they are full.

When one types more than three characters in the "Student" field,
one can select the name of a student. Once this is done, the print
button is hidden, and new actions become available to manage the
deposit(s) of the student.

## Restore the previous deposit number <a name="restaurer"></a>

![previous deposit]({static}../images/caution2.png){.shadowed}

For students whose deposit number is older than the usage of the program SLM,
the deposit number is set to zero; if one knows the previous deposit number,
for instance by reading it from a register previously written, it is possible
to enter this value into the field "Previous deposit number:". One can
optionally enter a date at format YYYY-MM-DD. When one validates, a
confirmation is requested, then the deposit number is updated in the database.
Older deposit numbers (whose value is less than 15000) are not written in
the book managed by SLM; however, they appear on printable documents, like the
tracking sheets, loan sheets, and the memorandum for lost books.

## Ordinary management <a name="gestion"></a>

When the chosen student has a non-zero deposit number, here is the
look of the screen:

![ordinary deposit]({static}../images/caution3.png){.shadowed}

Roles of action buttons are respectively:

  1. Cancel the deposit, so the student enters a status "no deposit"
  2. "Give back the deposit": to be done when the deposit is given back
     to the student, as a whole or a fraction. In the dialog which appears,
     one enters the amount which is given back, which can be different from
     the deposit's value, for example when a book has been lost
  3. Edit a comment: a dialog appears, which allows one to edit a
     short comment about the management of this student's deposit.
     *This comment does not appear in the printed book of deposits*.

## Series of deposits <a name="sequence"></a>

![series of deposits]({static}../images/caution4.png){.shadowed}

One student can have more than one mention in the book of deposits, with
distinct numbers. The screenshot above exemplified the case of a student
whose deposit #12400 has been given back, because they left the school,
thinking that they won't come back, and the deposit #15604 has been created
because of their re-subscription.


