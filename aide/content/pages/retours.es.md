---
Title: Devoluciones
slug: retours
layout: page
date: 2023-12-01
lang: es-es
translated: true
---

Esta página sirve para gestionar los retornos de libros en fin de año escolar.

## "Devoluciones" <a name="retours"></a>

Devoluciones de libros son muy parecidas a los préstamos ya descritos:
se precisa el nombre del alumno, y después se tecla o flashea los códigos
de libros devueltos.

![devoluciones]({static}../images/es-es/retours1.jpg){.shadowed}

A cada devolución de libro, la líneas de la tabla, inicialmente blancas,
se coloran:

- en verde, por defecto, cuando un libro fue entrado.
- en color naranja, cuando se modifica el estado final del libro después de
  su entrada, para significar una degradación. Se modifica gracias al menú
  desplegable en la columna "estado final", en la derecha.
- si el alumno declara un libro perdido, se marca la casilla de la columna
  "Perdido" y en este caso, la línea aparece roja.
  
**N. B.**: las modificaciones hechas en la tabla de devoluciones permanecen
accesibles y modificables tanto cuanto se quiere, si la modificación ocurre
el mismo día. Modificaciones que se pueden hacer son: desmarcar la casilla
"perdido" en una línea, anular una devolución (el botón para anular está
cerca de la fecha de devolución).

## "Misma clase" <a name="meme-classe"></a>

![alumnos de la misma clase]({static}../images/es-es/retours2.jpg){.shadowed}

En la parte derecha de la página web, hay una lista de alumnos de la misma clase.
Esta lista facilita algunas operaciones:

  - cada nombre de alumno en la lista es un enlace para devolver (o prestar)
    libros de este alumnos
  - lista de "clases previas" donde el alumno fue suscrito aparece.

En el contexto de devoluciones, la parte arriba de la zona "misma clase"
tiene dos botones: un con una flecha izquierda, par ir directamente a
la página de préstamos (para el alumno corriente), y el otro botón está
para declarar que de devuelve efectivamente el depósito.

