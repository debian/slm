---
Title: Depósitos
slug: caution
layout: page
date: 2023-09-13
lang: es-es
translated: true
---

Aquí está la captura de pantalla de la página de depósitos:

![depósitos]({static}../images/es-es/caution1.jpg){.shadowed}

El botón con icono de impresora es para descargar la ultima versión del
libro de depósitos al formato PDF. Este libro solamente cambia por adición de
nuevas líneas y nuevas páginas cuando lo necesita. Entonces se puede útilmente
imprimir las páginas completas del libro.

Cuando se entra tres letras o más en el campo de entrada "Alumno", se puede
escoger el nombre completo de un alumno. Después, el botón de impresión
desaparece, y nuevas acciones son posibles para gestionar el deposito o
los depósitos del alumno.

## Restaurar un número de depósito antiguo <a name="restaurer"></a>

![depósito antiguo]({static}../images/es-es/caution2.jpg){.shadowed}

Para alumnos que tienen un número de depósito anterior a la utilización
del programa SLM, el número de depósito es arreglado a cero; si se tiene el
número de depósito antiguo, por ejemplo escrito en un libro utilizado
previamente, se puede entrarlo en el campo de entrada
"Número de depósito antiguo". Se puede también precisar una fecha al
formato AAAA-MM-DD. Cuando se lo valida, una confirmación es preguntada,
y el nuevo número de depósito es registrado en la base de datos.
Los números antiguos de depósitos (que son inferiores a 15000) no
aparecen en el libro de depósitos gestionado por SLM; sin embargo,
aparecen en otros documentos imprimibles, como hojas de seguimiento,
hojas de préstamos, de devolución, o el memorándum en caso de perdida de
libros.

## Gestión ordinaria <a name="gestion"></a>

Cuando el alumno corriente tiene un número de depósito (diferente de cero),
la pantalla aparece así:

![depósito ordinario]({static}../images/es-es/caution3.jpg){.shadowed}

Los botones de acción sirven respectivamente a:

  1. Neutralizar el depósito, de tal modo que el alumno sea "sin depósito"
  2. "Devolver el depósito": se debe hacer lo cuando el depósito es devuelto
     al alumno, totalmente o parcialmente. en el dialogo que aparece, se
     precisa el valor de la suma devuelta, que puede ser diferente
     del valor del depósito, por ejemplo cuando un libro fue perdido
  3. Editar un comentario: un dialogo aparece, donde se puede editar un
     breve comentario relativo a la gestión del depósito del alumno.
     *el comentario no aparece en el libro de depósitos imprimido*.

## Depósitos sucesivos <a name="sequence"></a>

![depósitos sucesivos]({static}../images/es-es/caution4.jpg){.shadowed}

Un alumno puede tener más de una mención en el libro de depósitos, con
números distintos. La captura de pantalla arriba mostra el caso de un
alumno cuyo depósito N° 12400 fue devuelto, porque se fue de la escuela,
pensando que no regresaría, y el depósito N° 15604 fue creado cuando
regresó.

