---
Title: Loans
slug: prets
layout: page
date: 2023-12-01
lang: en-us
translated: true
---

This page is to manage loans of school books to students at the begin
of the school year.

## "Loans" <a name="prets"></a>

The loans page features three operations:

1. view the list of school books currently lent to a student,
2. add school books to lend them,
3. print a loan sheet, which the student must sign.

![loans]({static}../images/prets1.jpg){.shadowed}

**Here is the user manual:**

1. type three letters or more of the name or surname of the student:
   a drop-down list appears, where one can select the complete
   student's name; as soon as the student's name is selected, the
   list of books currently lent appear, and the focus goes to the
   input field for the book's number; then, one can type the book's number
   or flash its bar-code with the bar-code reader.
2. each time a book's number is entered, it is added to the list of new
   books to be lent, eventually after a confirmation request (for example,
   if one wants to lend two books with the same title to the same student).
3. when the list of new books to be lent is correct, one click on the
   save button, in order to add the books in the list of "registered loans".
   After that, one can still add new books as in stage 2, but it is no longer
   possible to remove books from the list.
4. finally, one clicks on the print button to create the printable loans
   sheet. That one is downloaded in PDF format. When the browser is well
   configured, one can read it and send it to the printer.

During step #2, one can still cancel lending a school book, as long as
the list of books to be lent has not been validated.

The loans sheet is made on the basis of books already lent; so one must
first validate the list of new books to lend, before asking for the
loans sheet.

## "Amendments" <a name="avenants"></a>

One can print an amendment when some new loans are added, and an older
loans sheet had been signed.

When new books to be lent are added at another, later date, the program
detects the case and gives the opportunity to print an amendment on the back of
an already signed loans sheet.

![loans]({static}../images/prets2.jpg){.shadowed}

The screenshot above displays two new buttons to print documents, and the
help sentence for the upper button: "Add an amendment for 1 book(s)".
The upper button is to print the amendment in the top of page, the second
button can be used if an first amendment is already printed in the top of
the page; in such a case the amendment would be printed in the bottom of
the page.

## "Same class" <a name="meme-classe"></a>

![students of the same class]({static}../images/prets3.jpg){.shadowed}

In the right part of the web page, there is a list of students from the
same class. This list provides a few features:

  - each student's names in the list is a link to lend (or return) books
    of this student
  - a list of "previous classes" where the students had been subscribed
    is mentioned.

In the context of loans, there are action buttons to print a membership card,
and to go directly to a book return.
