---
Title: The home page
slug: page-accueil
layout: page
date: 2023-04-11
lang: en-us
translated: true
---

Let's presume that SLM works as a remote server, providing web pages
at the (fake) address https://exemple.slm.org; when one
visits this website directly, one sees the home page:

![home page of SLM]({static}../images/page_accueil.jpg){.shadowed}

This page can be seen by anybody: one does not require an authentication
to access it. It is in the tab "About SLM" which is between tabs
"Parameters", "Stock/Inventory", "Loans" and "Returns" on the left,
and "Assistance" on the right. Those other tabs go to web pages which
require an authentication.

