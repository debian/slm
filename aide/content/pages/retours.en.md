---
Title: Returns
slug: retours
layout: page
date: 2023-12-01
lang: en-us
translated: true
---

This page is for book returns at the end of the school year.

## "Returns" <a name="retours"></a>

Book returns work much alike books loans detailed previously: one must
enter the student's name, and then type or flash bar-codes of returned books.

![returns]({static}../images/retours1.jpg){.shadowed}

As book returns are entered, the lines in the table, initially in
black and white, become colored:

- in green, by default, when a book has been entered.
- in orange, if after entering the book, the final state of the book is
  altered to notify a degradation. To alter it, make a modification in the
  drop-down menu in the column "final state", on the right.
- if the student declares a lost book, on checks the box in the "Lost"
  column, and then the line gets a red background.
  
**N. B.**: changes made in the table remain accessible and modifiable
as often as wanted, if one does it the same day. Possible modifications are:
uncheck the "lost" status, and cancel a return (the cancel button is near
the return date).

## "Same class" <a name="meme-classe"></a>

![students of the same class]({static}../images/retours2.jpg){.shadowed}

In the right part of the web page, there is a list of students from the
same class. This list provides a few features:

  - each student's names in the list is a link to return (or lend) books
    of this student
  - a list of "previous classes" where the students had been subscribed
    is mentioned.

In the context of returns, the upper part of the "Same class" area has
two buttons; an arrow at the left, to jump to the loans page, and the other
button to declare that the deposit will be given back actually.
