---
Title: La página principal
slug: page-accueil
layout: page
date: 2023-04-11
lang: es-es
translated: true
---

Supongamos que SLM funciona como servidor distante, y sirve sus páginas
web desde la (falsa) URL https://exemple.slm.org; cuando se visita
este sitio directamente, aparece la página principal:

![página principal de SLM]({static}../images/es-es/page_accueil.jpg){.shadowed}

Esta página es visible para todos: no necesita autenticación. Corresponde
a la pestaña "Acerca de SLM", colocada entre pestañas "Parámetros",
"Compras/inventario", "Préstamos" y "Devoluciones" a su izquierda, y
la pestaña "Apoyo" a su derecha. Las otras pestañas son para páginas que
necesitan una autenticación.
