AUTHOR = 'SLM'
SITENAME = 'SLM-Aide'
SITETITLE = '..:: SLM : Aide ::..'
# SITESUBTITLE = 'aide contextuelle (clic sur l\'étoile)'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'https://getpelican.com/'),
#         ('Python.org', 'https://www.python.org/'),
#         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
#         ('You can modify those links in your config file', '#'),)
LINKS = None

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)
SOCIAL = None

DEFAULT_PAGINATION = 10

# THEME = 'themes/elegant'
THEME = 'themes/genus'

LANDING_PAGE_TITLE = \
    "Manuel de l'utilisateur de SLM, « School Library Management »"


PAGE_PATHS = ['pages']
STATIC_PATHS = ['images', 'data', 'pages']
EXTRA_PATH_METADATA = {
    'images/favicon.png': {'path': 'favicon.png'}
}

READERS = {'html': None}

SLUGIFY_SOURCE = 'basename'

ARTICLE_URL = 'articles/{date:%Y}/{date:%b}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'articles/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'
PAGE_URL = 'pages/{slug}/index.html'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


PLUGINS = ['i18n_subsites',]

STATIC_TRANSLATIONS = {
    "Summary": "Sommaire",
    "Translations": "Traductions",
}

HIDE_LANG = 'fr'

I18N_SUBSITES = {
    'en-us': {
        'SITENAME': 'SLM-Help',
        'SITETITLE': '..:: SLM : Help ::..',
        'HIDE_LANG': 'fr',
        'STATIC_TRANSLATIONS': {
            "Summary": "Summary",
            "Translations": "Translations",
        },
    },
    'es-es': {
        'SITENAME': 'SLM-Ayuda',
        'SITETITLE': '..:: SLM : Ayuda ::..',
        'HIDE_LANG': 'fr',
        'STATIC_TRANSLATIONS': {
            "Summary": "Sumario",
            "Translations": "Traducciones",
        },
    },
    'fr-fr': {
        'SITENAME': 'SLM-Aide',
        'SITETITLE': '..:: SLM : Aide ::..',
        'HIDE_LANG': 'fr',
        'STATIC_TRANSLATIONS': {
            "Summary": "Sommaire",
            "Translations": "Traductions",
        },
    },
}

