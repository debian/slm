# SLM: School Library Management

- **Autres langues** : [English](README.md), [Español](LEAME.md)
- **Mode d'emploi, copies d'écran** : [voir l'aide contextuelle de SLM](https://slm-demo.freeduc.org/aide/index.html)
- **Démonstration** : envoyer un courriel à <georgesk@debian.org > avec le sujet « essayer SLM », pour avoir accès à un serveur de test.

-----------------------------------------

<img alt="salam : SML" src="Collaboration_logo_V2.svg" style="width: 150px; float: left; margin-right: 12px;">

Ce projet a été démarré en 2023 au lycée Jean Bart de Dunkerque, 
en France. Les livres scolaires y sont gérés par une association,
la « coopérative du lycée », animée par des bénévoles. Les élèves 
sont environ mille cinq cents à bénéficier de la location des livres 
gérés par la coopérative ; tous ces « clients » passent en moins 
d'une semaine, pour retirer leurs livres au début d'une année scolaire,
et reviennent en fin d'année rendre les livres qu'ils avaient loué.

## Bref historique

<img alt="livre - Gemasco" src="book-gemasco.png"  style="width: 150px; float: right; margin-left: 12px;">
<b>SLM</b> doit beaucoup à <b>[GeMaSco](https://openacademie.fr/gemasco/)</b>, 
le logiciel de gestion des manuels scolaires développé par
l'organisation [OpenAcademie](https://openacademie.fr/). Pour nommer
le projet <b>SLM</b>, on peut le prononcer « Salam » (سَّلَام, salām),
« Shalom » (שָׁלוֹם), ce qui se traduit tout simplement par « salut »,
mais signifie aussi bien « paix ». Gemasco est basé sur
[Microsoft Access](https://fr.wikipedia.org/wiki/Microsoft_Access), et il
consiste en un ensemble de routines écrites en Visual Basic, qui interagissent
avec dix-sept tables contenant les données de l'application.

## Migration GeMaSco → SLM

<img alt="Django-project logo" src="logo-django.svg" style="width: 150px; float: left; margin-right: 12px;">
Une migration des tables (remplies avec les prêts de livre faits en
septembre 2023) a été faite, en réécrivant leur structure comme des *classes*
héritant du système [Django](https://docs.djangoproject.com/fr/4.1/), puis
en procédant à l'importation des données, pour peupler une base de données
SQL. Après cette migration, il a été possible de définir des *vues* qui
sont autant de *pages web* basées sur des *modèles* et des règles
de style (la forme est séparée du fond). Une part de l'interaction entre
les pages web et l'utilisateur fait appel à des programmes en Javascript,
qui utilisent les bibliothèques [jQuery](https://jquery.com/) et
[jQuery-UI](https://jqueryui.com/).

