# SLM: School Library Management

- **Otros lenguajes** : [English](README.md), [Français](LISEZMOI.md)
- **Instrucciones de uso, capturas de pantalla** : [ver la ayuda contextual de SLM](https://slm-demo.freeduc.org/aide/index.html) (solamente en Francés actualmente)
- **Demostración** : enviar un e-mail a <georgesk@debian.org > con asunto "probar SLM", par tener acceso a un servidor de test.

-----------------------------------------

<img alt="salam : SML" src="Collaboration_logo_V2.svg" style="width: 150px; float: left; margin-right: 12px;">

Este proyecto arrancó el año 2023 en lycée Jean Bart, Dunkerque, 
en Francia. Ahí los libros escolares son administrados por una asociación,
la "cooperativa del liceo", dirigida por voluntarios. Los alumnos
están mil quinientos que benefician del préstamo de libros
administrados por la cooperativa; todos estos "clientes"pasan en menos
de una semana, para recocer sus libros empezando el año escolar,
y vuelven otra vez al fin del año para restituir los libros prestados.

## Breve historia

<img alt="livre - Gemasco" src="book-gemasco.png"  style="width: 150px; float: right; margin-left: 12px;">
<b>SLM</b> debe mucho a <b>[GeMaSco](https://openacademie.fr/gemasco/)</b>, 
el software de administración de libros escolares  desarrollado por
la organización [OpenAcademie](https://openacademie.fr/). Para nombrar
el proyecto <b>SLM</b>, se puede decir "Salam" (سَّلَام, salām),
"Shalom" (שָׁלוֹם), lo que puede ser traducido en "saludo",
pero también significa "paz". Gemasco está basado en
[Microsoft Access](https://fr.wikipedia.org/wiki/Microsoft_Access), y
es hecho de un conjunto de rutinas escritas en Visual Basic, interactuando
con diez y siete tablas que contienen los datos de la aplicación.

## Migración GeMaSco → SLM

<img alt="Django-project logo" src="logo-django.svg" style="width: 150px; float: left; margin-right: 12px;">
Una migración de las tablas (llenas con préstamos de libros hechos en
Septiembre 2023) fue exitosa, reinscribiendo su estructura como "clases"
heredando del sistema [Django](https://docs.djangoproject.com/fr/4.1/), pues
importando datos, para popular una base de datos
SQL. Después de esta migración, pudimos definir *vistas* que son
tantas *paginas web* basadas en *modelos* y reglas de estilo
(la forma y el fondo son separados). Parte de la interacción entre
las páginas web y el usuario llama programas escritos en Javascript,
con librerías [jQuery](https://jquery.com/) y
[jQuery-UI](https://jqueryui.com/).

